# Application to process custom json files, and to calculate some statistics on those files.
---

Here is the help output, with guides on how to use the application.


|Directory          | Description|
|-------------------|------------|
| examples/         | Source code examples for lectures or slides, etc.|
| iRememberApps/    | The MOOC apps |
| README.md         | This File. |

Here is an explation of the command line options you can choose

| short option | long option| Description|
|--------------|------------|------------|
|-h | --help              |        print this message. |
|-f \<file> | --inputfile \<arg>   |        File to process |
|-F \<file> | --fileLists \<arg>   |        File of Files to process |
|-o \<file> | --outputFile \<arg>  |        Write output to this file. (Optional, will output to console otherwise) |
|-p | --powerSet          |        Enable Power Set calculation of Attributes List. |

Here is a sample command using the application.

```
java -jar ./target/CloudStats-1.0-SNAPSHOT.jar -F ./../CloudStats/sample-files-list-to-calculate.txt -o myout2.csv -p
```

The -p option calculates all the possible combinations of the possible variables and allows you to determine which varibles are more important for which tests, etc.