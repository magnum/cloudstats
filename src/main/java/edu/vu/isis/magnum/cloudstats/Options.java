package edu.vu.isis.magnum.cloudstats;

public class Options {
	public static String[] TARGET_FIELDS = {"cpuUsage", "memory",
			"networkEth0", "cpu", "mean"};

	public static String[] TARGET_ORIGINAL_FIELDS = {"cpuIdle", "memory",
			"networkEth0", "cpu", "mean"};

	public static String[] COLUMNS_TO_PRINT = {"correlationCoefficient",
			"meanAbsoluteError", "errorRate", "relativeAbsoluteError",
			"rootRelativeSquaredError"};

	public static boolean hasTimestampToRemove = Boolean.getBoolean("false");

	public static String[] PERFORMANCE_FIELDS = {"cpuIdle", "memory",
			"networkEth0", "cpu"};
	public static String[] THROUGHPUT_FIELDS = {"count", "mean"};

	public static void setColumnsToPrint(String[] columns) {
		COLUMNS_TO_PRINT = columns;
	}

	public static void setTargetFields(String[] fields) {
		TARGET_FIELDS = fields;
	}

	public static void setPerformanceFields(String[] fields) {
		PERFORMANCE_FIELDS = fields;
	}

	public static void setThroughputFields(String[] fields) {
		THROUGHPUT_FIELDS = fields;
	}

	public static void setTargetOriginalFields(String[] fields) {
		TARGET_ORIGINAL_FIELDS = fields;
	}
}
