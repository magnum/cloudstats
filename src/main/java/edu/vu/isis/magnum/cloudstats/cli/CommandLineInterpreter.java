package edu.vu.isis.magnum.cloudstats.cli;

import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;

public class CommandLineInterpreter extends CommandLineInterpreterAbstractBase {

	/*
	 * Constants in case they are needed for Required-Logic
	 */
	public final static String fieldsOptionName = "fields";
	public final static String fileInputOptionName = "inputfile";
	public final static String fileListOptionName = "fileLists";
	public final static String outputFileOptionName = "outputFile";
	public final static String singleAttributesOptionName = "singleAttribute";
	public final static String testPowerSetOptionName = "powerSet";
	public final static String selectStatMethodOptionName = "statMethod";

	/**
	 * Determine if required options were given or not.
	 * 
	 * @return
	 */
	@Override
	final boolean customLogicForRequiredOptions() {
		return (hasParameter(fileInputOptionName) || hasParameter(fileListOptionName));
	}

	/**
	 * Create Options List
	 * 
	 * @return Options for the
	 */
	@Override
	protected Options createOptionsList() {
		Options options = new Options();

		Option fileOption = new Option("f", fileInputOptionName, true,
				"File to process");

		Option fileListOption = new Option("F", fileListOptionName, true,
				"File of Files to process");

		Option outputFileOption = new Option("o", outputFileOptionName, true,
				"Write output to this file. (Optional)");

		Option powerSetAttributeOption = new Option("p",
				testPowerSetOptionName, false,
				"Enable Power Set calculation of Attributes List.");

		// Not adding these right now, because future functionality isn't 100%
		// Option singleAttributeOption = new Option("c",
		// singleAttributesOptionName, false,
		// "Choose Single-Only Attribute for Calculations. (Optional)");

		// Option selectStatisticalMethodOption = new Option("m",
		// selectStatMethodOptionName, true,
		// "Select statistical method to use for calculations. (arg=> Linear|...)");

		// Option fieldsOption = new Option("s", fieldsOptionName, true,
		// 		"Select Fields to use in Calculations. (Optional)");

		options.addOption(fileOption);
		options.addOption(fileListOption);
		options.addOption(outputFileOption);
		options.addOption(powerSetAttributeOption);

		// Not adding these right now, because future functionality isn't 100%
		// options.addOption(fieldsOption);
		// options.addOption(singleAttributeOption);
		// options.addOption(selectStatisticalMethodOption);

		return options;
	}

}
