package edu.vu.isis.magnum.cloudstats.fileio;

import java.util.ArrayList;

import edu.vu.isis.magnum.cloudstats.Options;
import edu.vu.isis.magnum.cloudstats.stats.ArffFileData;
import edu.vu.isis.magnum.cloudstats.stats.CapturedPerfPointsData;

public class WebTestJsonReader {

	private static final String targetAttr = "count";

	ArffFileData fileData;

	private static final String timestampString = "timestamp";

	public WebTestJsonReader(String file, String testName) {

		fileData = new ArffFileData(testName);

		ArrayList<CapturedPerfPointsData> data = CustomJsonReader.getData(file);

		fileData.addNumericaAttribute(targetAttr);
		for (String attributeName : Options.TARGET_ORIGINAL_FIELDS) {
			fileData.addNumericaAttribute(attributeName);
		}

		for (CapturedPerfPointsData rowData : data) {
			fileData.addDataRow(rowData.values);
		}

		fileData.removeParameterFromData(timestampString);

		fileData.convertCPUIdleToCPUUtilization();

	}

	public String getArffString(ArrayList<String> attributesToUse) {
		return fileData.getOutputFileString(attributesToUse);
	}

}
