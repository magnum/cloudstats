package edu.vu.isis.magnum.cloudstats.cli;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import edu.vu.isis.magnum.cloudstats.Options;
import edu.vu.isis.magnum.cloudstats.Utils;

public class CommandLineProcessor {

	CommandLineInterpreter mCLI;

	public CommandLineProcessor(CommandLineInterpreter cli) {
		mCLI = cli;
	}

	public ArrayList<String> getJsonFilesToProcess() throws IOException {
		ArrayList<String> jsonFilesList = new ArrayList<String>();

		if (mCLI.hasParameter(CommandLineInterpreter.fileInputOptionName) == true) {
			/*
			 * Single input file given
			 */
			String inputFile = mCLI
					.getParameter(CommandLineInterpreter.fileInputOptionName);
			jsonFilesList.add(inputFile);
		} else if (mCLI.hasParameter(CommandLineInterpreter.fileListOptionName) == true) {
			/*
			 * File with File paths per line given
			 */
			String jsonFileListPath = mCLI
					.getParameter(CommandLineInterpreter.fileListOptionName);

			BufferedReader br = null;
			try {
				br = new BufferedReader(new FileReader(jsonFileListPath));
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			}

			String sCurrentLine;
			while ((sCurrentLine = br.readLine()) != null) {
				// add if not comment line
				if (sCurrentLine.startsWith("#") == false) {
					jsonFilesList.add(sCurrentLine);
				}
			}
			br.close();
		}
		return jsonFilesList;
	}

	public ArrayList<List<String>> getAttributesToCalculate() {
		ArrayList<List<String>> tests = new ArrayList<List<String>>();

		ArrayList<String> attributesToUse = new ArrayList<String>(
				Arrays.asList(Options.TARGET_FIELDS));
		String powerOption = CommandLineInterpreter.testPowerSetOptionName;
		// calculate power set if true
		if (mCLI.hasParameter(powerOption) == true) {
			// create new copy, with same values
			ArrayList<String> attributesToUse_Temp = new ArrayList<String>(
					attributesToUse);
			Collections.copy(attributesToUse_Temp, attributesToUse);
			attributesToUse_Temp.remove("count");

			tests = (ArrayList<List<String>>) Utils
					.calculatePowerSet(attributesToUse_Temp);
			tests.remove(0); // remove empty set
		} else {
			String singleAttrOption = CommandLineInterpreter.singleAttributesOptionName;
			if (mCLI.hasParameter(singleAttrOption)) {
				for (String string : attributesToUse) {
					ArrayList<String> temp = new ArrayList<String>();
					temp.add(string);
					tests.add(temp);
				}

			} else {
				tests.add(attributesToUse);
			}
		}

		return tests;
	}

	public String getOutputFileName() {
		if (mCLI.hasParameter(CommandLineInterpreter.outputFileOptionName)) {
			String outputFilePath = mCLI
					.getParameter(CommandLineInterpreter.outputFileOptionName);
			return outputFilePath;
		} else {
			return null;
		}
	}

	public ArrayList<String> getColumnsToPrint() {
		ArrayList<String> colToPrint = new ArrayList<String>(
				Arrays.asList(Options.COLUMNS_TO_PRINT));
		return colToPrint;
	}

}
