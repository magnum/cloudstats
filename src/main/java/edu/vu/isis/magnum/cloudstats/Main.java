package edu.vu.isis.magnum.cloudstats;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import edu.vu.isis.magnum.cloudstats.cli.CommandLineInterpreter;
import edu.vu.isis.magnum.cloudstats.cli.CommandLineProcessor;
import edu.vu.isis.magnum.cloudstats.fileio.CsvProcessor;
import edu.vu.isis.magnum.cloudstats.fileio.WebTestJsonReader;
import edu.vu.isis.magnum.cloudstats.stats.StatisticsCalculator;

public class Main {

	public static void main(String[] args) throws Exception {

		/*
		 * Create main to do logic, and cli to parse arguments
		 */
		Main main = new Main();
		CommandLineInterpreter cli = new CommandLineInterpreter();
		// Parse arguments
		cli.parseCommandLineArgs(args);
		// Extract relevant results from the parsed command line args
		CommandLineProcessor processor = new CommandLineProcessor(cli);
		ArrayList<String> colToPrint = processor.getColumnsToPrint();
		ArrayList<String> jsonFilesList = processor.getJsonFilesToProcess();
		ArrayList<List<String>> tests = processor.getAttributesToCalculate();
		String outputFile = processor.getOutputFileName();

		CsvProcessor csvProcessor = new CsvProcessor();
		String csvHeaderString = csvProcessor.getCsvHeader(cli);

		main.processData(tests, jsonFilesList, csvHeaderString, colToPrint,
				outputFile);
	}

	public void processData(ArrayList<List<String>> tests,
			ArrayList<String> jsonFilesList, String csvHeaderString,
			ArrayList<String> colToPrint, String outputFile) throws Exception {
		String csvFile = csvHeaderString;
		for (List<String> list : tests) {
			list.add(0, "count");
			for (String filePath : jsonFilesList) {
				csvFile += list.size() + "; ";
				int startChar = (filePath.length() - (filePath.length() / 2));
				csvFile += getCSVLine(
						filePath.subSequence(startChar, filePath.length())
								.toString() + list, filePath,
						(ArrayList<String>) list, colToPrint);
			}
		}
		this.outputBasedOnParameters(outputFile, csvFile);
	}

	public void outputBasedOnParameters(String outputFilePath, String csvString)
			throws IOException {
		if (outputFilePath != null) {
			BufferedWriter bf = new BufferedWriter(new FileWriter(
					outputFilePath));
			bf.write(csvString);
			bf.flush();
			bf.close();
		} else {
			System.out.println(csvString);
		}
	}

	public String getJsonFilePath(CommandLineInterpreter cli) throws Exception {
		if (cli.hasParameter(CommandLineInterpreter.fileInputOptionName)) {
			return cli.getParameter(CommandLineInterpreter.fileInputOptionName);
		} else {
			throw new Exception("Input JSON File not provided.");
		}
	}

	public static String getCSVLine(String testName, String filePath,
			ArrayList<String> attributesToUse, ArrayList<String> colToPrint)
			throws Exception {
		WebTestJsonReader reader = new WebTestJsonReader(filePath, testName);
		String file = reader.getArffString(attributesToUse);

		HashMap<String, Double> results = StatisticsCalculator
				.calculateStatistics(testName, file);

		CsvProcessor csvProcessor = new CsvProcessor();
		csvProcessor.setDivString("; ");
		csvProcessor.setColumnsToPrint(colToPrint);
		return csvProcessor.createCsvLine(testName, results);

	}

}
